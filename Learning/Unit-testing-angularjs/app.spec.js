var expect = require('chai').expect;
var angular = require('angular');
describe('filters', function(){
	beforeEach(angular.module('app'));
	describe('should reverse a string', function(){
		it("should reverse", inject(function(reverseFilter){
			expect(reverseFilter("ABCE")).to.be.equal("ECBA");
		}))
	})
})