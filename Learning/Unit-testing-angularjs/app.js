var app = angular.module('app', []);

app.factory('Data', function() {
    return {
        message: "I am data from service"
    };
})
app.filter("reverse", function(Data){
	return function(msg) {
        return "'" + msg.split("").reverse().join("") + " ' is reverse of ' " + Data.message + "'";
    }
})

app.controller('FirstCtrl', function FirstCtrl($scope, Data) {
    $scope.data = Data;
})

app.controller('SecondCtrl', function SecondCtrl($scope, Data) {
    $scope.data = Data;

    $scope.reverseMessage = function(msg) {
        return msg.split("").reverse().join("");
    }
})