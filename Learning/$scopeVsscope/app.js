var app = angular.module('app', []);

app.controller('MyCtrl', function ($scope) {//short cut apporoach but the longcut one is better
	console.log($scope);
})

app.directive('myDirective', function () {
	return {
		//scope: {},//isolated scope
		link: function (scope) {//ordr matter.
			console.log(scope);
		}
	};
})