var app = angular.module('app', []);
app.controller('AppCtrl', function($scope){
	$scope.loadMoreTweets = function(){
		alert("loading more tweets");
	}

	$scope.delete = function(){
		alert("delete tweets!!");
	}
})
//By using this way the directive becomes reusable.
app.directive("enter", function(){
	return function(scope, element, attrs){
		element.bind("mouseenter", function(){
			//scope.loadMoreTweets();//Not the best way to do it because it's not sure which scope is it.
			//scope.$apply("loadMoreTweets()");//Gives the same result but still should not be doing it.
			scope.$apply(attrs.enter);//Best way to do it.
		})
	}
})

app.directive("leave", function(){
	return function(scope, element, attrs){
		element.bind("mouseleave", function(){
			element.removeClass(attrs.enter);
		})
	}
})
