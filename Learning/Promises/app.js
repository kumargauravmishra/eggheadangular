var app = angular.module('app', ["ngRoute"]);

app.config(function( $routeProvider){
	$routeProvider.when('/', {
		templateUrl: "app.html",
		controller: "AppCtrl"
	})
})

app.controller('AppCtrl', function ($scope, $q) {
	var defer = $q.defer();
	defer.promise.then(function (weapon) {
		alert(" Vergil , you can have my " + weapon);
		return "yamato";
	}).then(function (weapon){
		alert(" then can i have your : " + weapon);
	});
	defer.resolve("Rebelian");
	$scope.model = {
		message: "this is home page"
	}
})