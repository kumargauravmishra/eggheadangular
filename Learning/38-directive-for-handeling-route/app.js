var app = angular.module('app', ["ngRoute"]);

app.config(function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: "app.html",
        controller: "ViewCtrl",
        resolve: {
            loadData: viewCtrl.loadData
        }
    })
})
app.directive('error', function($rootScope) {
    return {
        restrict: 'E',
        template: '<div class="alert-box alert" ng-show="isError">Error!!!</div>',
        link: function(scope) {
            $rootScope.$on("$routeChangeError", function(event, current, previous, rejection) {
                scope.isError = true;
            });
        }
    };
})
app.controller('AppCtrl', function($rootScope) {
    $rootScope.$on("$routeChangeError", function(event, current, previous, rejection) {
        alert(rejection);
    });
});

var viewCtrl = app.controller('ViewCtrl', function($scope, $route) {
    console.log($route);
    $scope.model = {
        message: "I will show you how great I am."
    }
});

viewCtrl.loadData = function($q, $timeout) {
    var defer = $q.defer();
    $timeout(function() {
        defer.reject("Failed to load the routes");//error conditon and error handling when promise got rejected.
        //defer.resolve("routes loadded successfully");
    }, 2000);
    return defer.promise;
};