describe( "Hello world", function () {
	var element;
	var $scope;
	var $compile;
	var _$rootScope;
	beforeEach( module( "app" ) );
	beforeEach( inject( function ( _$compile_, _$rootScope_ ) {
		$rootScope = _$rootScope_;
		$compile = _$compile_;
		$scope = $rootScope;
		$scope.data = {
			message: "Hello"
		};
		element = angular.element( "<div eh-simple='data'>{{2 + 2}}</div>" );
		$compile( element )( $rootScope );
	} ) )

	it( 'should equal 4', function () {
		$scope.$digest();
		expect( element.html() ).toBe( "4" );
	} )

	describe( "ehSimple", function () {
		it( "should add a class of plain", function () {
			expect( element.hasClass( "plain" ) ).toBe( true );
		} )

		it( "should respond to a click", function () {
			element.triggerHandler( 'click' );
			expect( element.isolateScope().clicked ).toBe( true );
		} )

		it( "should update both scopes on click", function () {
			element.triggerHandler( 'click' );
			expect( element.isolateScope().ehSimple.message ).toBe( "Hello world" );
		} )
	} )
} )
