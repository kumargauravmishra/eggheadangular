var app = angular.module('app', []);

app.factory('game', function () {

	return {
		title: "Devil May Cry"
	};
})

//Injectors way
app.controller('AppCtrl', function($scope, $injector) {
    $injector.invoke(function(game){
        alert(game.title + " is Awesome !!!");
        $scope.title = ((game && game.title) ? (game.title + " is Awesome!") : (" I am Awesome!!!"));
    })
    
});
//Can be done this way
/*
app.controller('AppCtrl', function($scope, game) {
    $scope.title = ((game && game.title) ? (game.title + " is Awesome!") : (" I am Awesome!!!"))
});
*/