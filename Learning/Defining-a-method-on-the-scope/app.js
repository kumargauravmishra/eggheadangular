var app = angular.module('app', []);

app.factory('Data', function() {
    return {
        message: "I'm data from service"
    };
})

app.controller('FirstCtrl', function FirstCtrl($scope, Data) {
    $scope.data = Data;
})

app.controller('SecondCtrl', function SecondCtrl($scope, Data) {
    $scope.data = Data;

    $scope.reverseMessage = function(msg) {
        return msg.split("").reverse().join("");
    }
})