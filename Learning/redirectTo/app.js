var app = angular.module('app', ["ngRoute"]);

app.config(function( $routeProvider){
	$routeProvider.when('/', {
		templateUrl: "app.html",
		controller: "AppCtrl"
	})
	.when('/pizza/:crust/:toppings', {
		redirectTo: function(routeParams, path, search){
			alert("pizza info = " + routeParams.crust + " = " + routeParams.toppings + " And Path = " +  path)
			
			return "/" + routeParams.crust
		}
	})
	.when('/deep', {
		templateUrl: "error.html"
	})
	.otherwise({
		redirectTo: "/"
	})
})

app.controller('AppCtrl', function ($scope, $routeParams) {
	$scope.model = {
		message: "this is home page"
	}
})