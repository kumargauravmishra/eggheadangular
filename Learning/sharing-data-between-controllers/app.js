var app = angular.module('app', ["ui.router"]);

app.config(function config($stateProvider) {
    $stateProvider.state("index", {
        url: "",
        controller: "FirstCtrl as first",
        templateUrl: "./templates/first.html"
    })
    $stateProvider.state("second", {
        url: "/second",
        controller: "SecondCtrl as second",
        templateUrl: "./templates/second.html"
    })
});

app.service('greeting', function Greetings() {
	var greeting = this;
	greeting.message = "Hello";
});
app.controller('FirstCtrl', function FirstCtrl(greeting) {
    var first = this;
    first.greeting = greeting;
});

app.controller('SecondCtrl', function SecondCtrl(greeting) {
    var second = this;
    second.greeting = greeting;
});