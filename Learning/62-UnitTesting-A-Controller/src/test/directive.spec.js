describe( "Hello world", function () {
	var $controller;
	var appCtrl;
	beforeEach( module( "app" ) );
	beforeEach( inject( function ( _$controller_ ) {
		$controller = _$controller_;
		appCtrl = $controller( "AppCtrl" );
	} ) )

	describe( "AppCtrl", function () {
		it( "should have a message of hello", function () {
			expect( appCtrl.message ).toBe( "Hello" );
		} )
	} )
} )
