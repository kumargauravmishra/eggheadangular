var app = angular.module('app', ["ngRoute"]);

app.config(function( $routeProvider){
	$routeProvider.when('/', {
		templateUrl: "app.html",
		controller: "AppCtrl"
	})
	.when('/pizza', {
		template: "YUM!!!"
	})
	.otherwise({
		template: "404 Not Found"
	})
})

app.controller('AppCtrl', function ($scope, $route) {
	$scope.model = {
		message: "This is my app!!!"
	}
})