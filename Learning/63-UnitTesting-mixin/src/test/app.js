var app = angular.module( "app", [] );

app.factory( "mixinto", [ 'mixin', function ( mixin ) {
  var mixinto = {};
  angular.extend( mixinto, mixin );
  return mixinto;
} ] )

app.factory( 'mixin', function () {
  return {
    cool: true
  }
} );
