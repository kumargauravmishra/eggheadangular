//Use this type of code if you want to minify it.
var app = angular.module("app", []);

app.service("Store", function() {
    this.products = {
        item: "apple"
    };
});

app.controller("AppCtrl", [ "$scope", "Store", function(p, o) {
    p.products = o.products;
} ]);