var app = angular.module('app', []);

app.service('Store', function () {
	this.product = {item: "apple"}
});

app.controller('AppCtrl', function ($scope, Store) {
	$scope.products = Store.products;
});