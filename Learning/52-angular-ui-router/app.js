var app = angular.module('app', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider){
	$stateProvider.state('home',{
		url: '/home',
		templateUrl: "home.html"
	});
	$stateProvider.state('list',{
		url: '/list',
		templateUrl: "list.html"
	});
	$stateProvider.state('list.item',{
		url: "/:item",
		templateUrl: "templates/list.item.html",
		controller: function($scope, $stateParams){
			$scope.item = $stateParams.item
		}
	});

});


app.controller('AppCtrl', function ($scope) {
	$scope.shoppingList = [
		{name: "Milk"},
		{name: "Eggs"},
		{name: "Breads"},
		{name: "Cheese"},
		{name: "Ham"}
	];

	$scope.selectedItem = function(selectedItem){
		_($scope.shoppingList).each(function(item){
			item.selected = false;
			if(selectedItem === item){
				selectedItem.selected = true;
			}
		});
	};	
});