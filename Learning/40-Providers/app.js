var app = angular.module('app', []);

//Best way to use factory is this one and this also have the flixibelity to change the attributes after definition.
app.provider("game", function() {
    var type;
    return {
        setType: function(value) {
            type = value
        },
        $get: function() {
            return {
                title: type ? type : "Devil May Cry"
            }
        }
    }
});

app.config(function(gameProvider){
	gameProvider.setType("Stalker(Shadow of Cornobyle)");
});

//This also works but  both have it's limitations
/*
app.config(function($provide) {
    $provide.provider("game", function() {
        return {
            $get: function() {
                return {
                    title: "Devil May Cry"
                }
            }
        }
    })
})
*/
//We can use above one or the below one but the function of both are same. 
//And The below one also works internally as the above one .
/*
app.factory('game', function () {

	return {
		title: "Devil May Cry"
	};
})
 */


app.controller('AppCtrl', function($scope, game) {
    $scope.title = ((game && game.title) ? (game.title + " is Awesome!") : (" I am Awesome!!!"))
});