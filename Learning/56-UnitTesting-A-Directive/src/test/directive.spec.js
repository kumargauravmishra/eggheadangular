describe( "Hello World", function () {
	var element;
	var $scope;
	beforeEach( module( "app" ) )
	beforeEach( inject( function ( $compile, $rootScope ) {
		$scope = $rootScope;
		element = angular.element( "<div eh-simple>{{2 + 2}}</div>" );
		element = $compile( element )( $scope );
		console.log( element );
	} ) );

	it( "should equal 4", function () {
		$scope.$digest()
		expect( element.html() ).toBe( "4" );
	} )

	describe( "Simple Directive Unit Test", function () {
		it( "should add a class of plain", function () {
			expect( element.hasClass( "plain" ) ).toBe( true );
		} )
	} )
} )
