var app = angular.module('app', []);

var stuff = {};

stuff.controllers = {};

stuff.controllers.AppCtrl = function($scope){
	this.sayHi = function(){
		alert("hi");
	}
	return $scope.AppCtrl = this;
}

stuff.directives = {};
stuff.directives.panel = function(){
	return {
		restrict: "E",

	}
}
app.controller(stuff.controllers);
app.directive(stuff.directives);

/*
	Note: It's nice to have this approach but this approach does
	not work with filters because filter does not take arguments.
*/
