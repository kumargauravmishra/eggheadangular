var app = angular.module("app", []);

app.directive('country', function () {
	return {
		restrict: 'E',
		controller: function(){
			this.makeAnnouncement = function(message){
				alert("Country Directive : " + message);
			}
		}
	};
});


app.directive('state', function () {
	return {
		restrict: 'E',
		controller: function(){
			this.makeLaw = function(law){
				alert("Law : " + law)
			}
		}
	};
});


app.directive('city', function () {
	return {
		restrict: 'E',
		require: "^country",
		link: function(scope, element, attrs, countryCtrl){
			countryCtrl.makeAnnouncement("This city rocks !!!");
		}
	};
});

app.directive('village', function () {
	return {
		restrict: 'E',
		require: "^state",
		link: function (scope, element, attrs, stateCtrl) {
			stateCtrl.makeLaw("Make All roads in 15 days.");
		}
	};
});

app.directive('experiment', [function () {
	return {
		restrict: 'E',
		require: ["^country", "^state"],
		link: function (scope, element, attrs, ctrl) {
			ctrl[0].makeAnnouncement("I will win, because i say so.");
			ctrl[1].makeLaw("From now onwards i will never loose");
		}
	};
}])