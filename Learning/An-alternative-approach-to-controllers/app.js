var app = angular.module("app",[]);

app.controller('AppCtrl', function ($scope) {
	/*
	$scope.sayHi = function(){
		alert("hi");
	}//conventional approach but not the best one.
	*/

	//New approach
	this.sayHi = function(){
		alert("hi");
	}
	return $scope.AppCtrl = this;
})
