var express = require( 'express' );
var bodyParser = require( 'body-parser' );
var http = require( 'http' );
var cors = require( 'cors' );

var app = express();

app.use( bodyParser() );
app.use( cors() );
app.set( 'port', 9000 );
var data = [
  {
    "firstName": "Kumar",
    "lastName": "Mishra"
  },
  {
    "firstName": "pride",
    "lastName": "Supermacy"
  },
  {
    "firstName": "Snake",
    "lastName": "007"
  },
  {
    "firstName": "Kritos",
    "lastName": "Wazasky"
  },

]
app.get( '/users', function ( req, res ) {
  res.send( data );
} );

app.post( '/users', function ( req, res ) {
  console.log( JSON.stringify( req.body ) );
  data.push( req.body )
  res.send( data );
} );

http.createServer( app ).listen( app.get( 'port' ), function () {
  console.log( 'Express server listening on port : ' + app.get( 'port' ) );
} )
