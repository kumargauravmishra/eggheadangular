var app = angular.module('app', ["ngRoute"]);

app.config(function( $routeProvider){
	$routeProvider.when('/', {
		templateUrl: "app.html",
		controller: "AppCtrl"
	})
})

app.controller('AppCtrl', function ($scope, $route) {
	/*$route.routes["/"] = {
		templateUrl: "app.html",
		controller: "AppCtrl"
	}*///This is perfactly legal but it cames way to late in the process so does not work.
	$scope.model = {
		message: "This is my app!!!"
	}
})