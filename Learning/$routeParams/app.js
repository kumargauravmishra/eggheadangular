var app = angular.module('app', ["ngRoute"]);

app.config(function( $routeProvider){
	$routeProvider.when('/:to/:message', {
		templateUrl: "app.html",
		controller: "AppCtrl"
	})
	.when('/pizza', {
		template: "YUM!!!"
	})
	.otherwise({
		template: "404 Not Found"
	})
})

app.controller('AppCtrl', function ($scope, $routeParams) {
	$scope.model = {
		message: "To : " + $routeParams.to + " " + "message : " + $routeParams.message 
	}
})